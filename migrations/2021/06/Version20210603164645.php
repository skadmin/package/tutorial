<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210603164645 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tutorial DROP INDEX UNIQ_C66BFFE9B168B8C0, ADD INDEX IDX_C66BFFE9B168B8C0 (prev_id)');
        $this->addSql('ALTER TABLE tutorial DROP INDEX UNIQ_C66BFFE9AA23F6C8, ADD INDEX IDX_C66BFFE9AA23F6C8 (next_id)');

        $translations = [
            ['original' => 'grid.tutorial.overview.name.prev: ', 'hash' => '9db816039ecd11d768db556ebda75591', 'module' => 'admin', 'language_id' => 1, 'singular' => 'předchozí:', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.tutorial.overview.name.next: ', 'hash' => '16b95b19a99989e1084d679ae05e5113', 'module' => 'admin', 'language_id' => 1, 'singular' => 'následující:', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.tutorial.overview.action.remove', 'hash' => '22cf7419cbfbe5675a7aeab787d951c6', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.tutorial.overview.action.remove.title', 'hash' => 'ea08e7e01824a5e93207ea72a01aafc9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Smazat', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.tutorial.overview.action.remove.confirmation - %s', 'hash' => '717c17c0f14f3287593b71998a4b3227', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Opravdu chcete smazat návod "%s"?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.tutorial.overview.action.flash.remove.success "%s"', 'hash' => '6a661063f67dcdbb8a9bb91ea7d859cf', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Návod "%s" byl úspěšně odebrán.', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tutorial DROP INDEX IDX_C66BFFE9B168B8C0, ADD UNIQUE INDEX UNIQ_C66BFFE9B168B8C0 (prev_id)');
        $this->addSql('ALTER TABLE tutorial DROP INDEX IDX_C66BFFE9AA23F6C8, ADD UNIQUE INDEX UNIQ_C66BFFE9AA23F6C8 (next_id)');
    }
}
