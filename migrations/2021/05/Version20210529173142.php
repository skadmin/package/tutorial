<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210529173142 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tutorial ADD prev_id INT DEFAULT NULL, ADD next_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tutorial ADD CONSTRAINT FK_C66BFFE9B168B8C0 FOREIGN KEY (prev_id) REFERENCES tutorial (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE tutorial ADD CONSTRAINT FK_C66BFFE9AA23F6C8 FOREIGN KEY (next_id) REFERENCES tutorial (id) ON DELETE SET NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C66BFFE9B168B8C0 ON tutorial (prev_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C66BFFE9AA23F6C8 ON tutorial (next_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tutorial DROP FOREIGN KEY FK_C66BFFE9B168B8C0');
        $this->addSql('ALTER TABLE tutorial DROP FOREIGN KEY FK_C66BFFE9AA23F6C8');
        $this->addSql('DROP INDEX UNIQ_C66BFFE9B168B8C0 ON tutorial');
        $this->addSql('DROP INDEX UNIQ_C66BFFE9AA23F6C8 ON tutorial');
        $this->addSql('ALTER TABLE tutorial DROP prev_id, DROP next_id');
    }
}
