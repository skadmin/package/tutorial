<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210530042700 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $translations = [
            ['original' => 'tutorial.overview', 'hash' => '156f81604359e9604bdab25014859910', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Návody', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tutorial.overview.title', 'hash' => '4b6c7d574cdb545c082829b65ff41e51', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Návody|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.tutorial.overview.tags', 'hash' => '0fc379d132b681b42aa080ffa6bb5c72', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Kategorie', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.tutorial.overview.action.overview-tag', 'hash' => 'dae7413db682e1e1c1c08c13e1816e4f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přehled kategorií', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.tutorial.overview.action.new', 'hash' => '81b5c914dbb56733e9e022b13e9b3817', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit návod', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.tutorial.overview.name', 'hash' => '60549b9dbae24be38faf5c2344f855f6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.tutorial.overview.is-active', 'hash' => '38b732e7f46b95cc2fa29f94432f4a37', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial.edit.title', 'hash' => '9b8868f71d3906181fefdb3dc0040331', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení návodu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial.edit.prev.chosen-placeholder-text', 'hash' => '365341d170e537d29f6268478a4659fe', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial.edit.prev.chosen-no-result-text', 'hash' => '166004e07a8a50bf6778613cac106f34', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Nebyl nalezen žádný návod: ', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial.edit.next.chosen-placeholder-text', 'hash' => 'db88faa30beb33e6a0329a77641c806b', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial.edit.next.chosen-no-result-text', 'hash' => '245bff9d2320ab5060c985dba583e88f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Nebyl nalezen žádný návod: ', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial.edit.name', 'hash' => 'dbc32321c375b8e25f9c3fdfac700fd8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial.edit.name.req', 'hash' => '465e8bb424d2409d548bd6709aadbffb', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial.edit.value', 'hash' => 'b89f6a8ca0bc2e89fec46066b45b3b42', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Video', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial.edit.value.title', 'hash' => 'aea9ea80d5aa80c5b0af4a3a8fd97601', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Jak zadat video?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial.edit.value.title-content', 'hash' => 'e4abbd1e1d0ed19d3c2f17b119d73320', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Video používáme z youtube a stačí zadat jen jeho kód (například "7jMlFXouPk8"), ale můžete vložit celou url adresu a systém zkusí kód zjistit sám.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial.edit.prev', 'hash' => 'e59ec8469e93c261549b249c8d5edb00', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Předchozí návod', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial.edit.next', 'hash' => '38fa2a80761657895f2d1424a162f5bd', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Následující návod', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial.edit.tags', 'hash' => '52d617b3c41173aff3d64060a9b0a0f2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Kategorie', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial.edit.tags.placeholder', 'hash' => '69a003c190a2cf71ef47d50dbfdec91c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vyberte libovolný počet kategorií', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial.edit.tags.no-result', 'hash' => '3e118450cef0de2a393e3b7e583c2cc8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Nebyla nalezena kategorie: ', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial.edit.content', 'hash' => 'b2b3dc8e10eff95c93260560771e1a95', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial.edit.is-active', 'hash' => 'b6839f70456b3acdb33ff95b57129a50', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial.edit.send', 'hash' => '445428e2ecc166ba261424a670b84b75', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial.edit.send-back', 'hash' => '1b4bb391f94019c950e60a273aa3beb5', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial.edit.back', 'hash' => '7b1072b91776f5a7190c7cec3be50d7f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial.edit.title - %s', 'hash' => '42f1c740337ed7175dc6c79ec5a7f62f', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace návodu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial.edit.flash.success.create', 'hash' => '322db92d02399ac41806cdce7592fa19', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Návod byl úspěšně založen.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial.edit.flash.success.update', 'hash' => 'c4cb359227f018bed0a9d92ab8715fb3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Návod byl úspěšně upraven.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'tutorial.overview-tag.title', 'hash' => '5e1b580ac4f7894f8fe5c7a89964e585', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Kategorie návodů|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.tutorial.overview-tag.action.overview', 'hash' => 'cb1c80277a6c5eb5e7cdd5a5c5ddaa40', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přehled návodů', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.tutorial.overview-tag.action.new', 'hash' => '46031fae350852ade2cae851622ba17c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit kategorii', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.tutorial.overview-tag.name', 'hash' => 'e68792deebfdd19321cf6f78b7b29493', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.tutorial.overview-tag.color', 'hash' => 'ab55476acacd58f8a21ea75dfd15db38', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Barva', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.tutorial.overview.action.edit', 'hash' => '9cbd1f73f1b766b1b4e0322f5685e822', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial-tag.edit.title', 'hash' => '3894f7ee172c2948f67d3a93df0d1ba2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení kategorie návodů', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial-tag.edit.name', 'hash' => 'c569c7cae610c967dd545d23b77c9b34', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial-tag.edit.name.req', 'hash' => '0d3006e36bfddd48c3cd4b2bf99c176a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial-tag.edit.color', 'hash' => '9ec081ba6a894db5bfa5f6d4b0bf1b2c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Barva', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial-tag.edit.content', 'hash' => '4a331a9bc84b91d32975ec5b2dcee355', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial-tag.edit.is-active', 'hash' => 'dda3fcc78fcd92a118802b78ea8a8758', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial-tag.edit.send', 'hash' => '6c7faa3422c5e388068da57386823cdd', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial-tag.edit.send-back', 'hash' => '477151badea962d8cd26d290da1496db', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial-tag.edit.back', 'hash' => '008127f2616ac6a29356e05e2d68a3da', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial-tag.edit.title - %s', 'hash' => 'debe93a09aedf4408af7c3aac97dec98', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace kategorie návodu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial-tag.edit.flash.success.create', 'hash' => '5339d18527774ffc6ea3f9b15161300e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Kategorie návodu byla úspěšně založena.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.tutorial-tag.edit.flash.success.update', 'hash' => 'd709811e13953a3dd546d6768f64cd4e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Kategorie návodu byla úspěšně upravena.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.tutorial.overview-tag.action.edit', 'hash' => 'ac74dd0b2458a8870b79e0dd5c64989c', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.tutorial.overview.name.prev: ', 'hash' => '9db816039ecd11d768db556ebda75591', 'module' => 'admin', 'language_id' => 1, 'singular' => 'předchozí:', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.tutorial.overview.name.next: ', 'hash' => '16b95b19a99989e1084d679ae05e5113', 'module' => 'admin', 'language_id' => 1, 'singular' => 'následující:', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema) : void
    {
    }
}
