<?php

declare(strict_types=1);

namespace Skadmin\Tutorial\Doctrine\Tutorial;

use App\Model\Doctrine\User\User;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Nette\Utils\Strings;
use Skadmin\Tutorial\Model\Helper;
use SkadminUtils\DoctrineTraits\Entity;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Tutorial
{
    public const TYPE_TEXT          = 'text';
    public const TYPE_VIDEO_YOUTUBE = 'video-youtube';

    public const TYPES = [
        self::TYPE_TEXT          => self::TYPE_TEXT,
        self::TYPE_VIDEO_YOUTUBE => self::TYPE_VIDEO_YOUTUBE,
    ];

    use Entity\BaseEntity;
    use Entity\IsActive;
    use Entity\WebalizeName;
    use Entity\Content;
    use Entity\Sequence;

    #[ORM\Column]
    private string $type = self::TYPE_TEXT;

    #[ORM\Column]
    private string $value = '';

    #[ORM\ManyToOne(targetEntity: Tutorial::class)]
    #[ORM\JoinColumn(onDelete: 'set null')]
    private ?Tutorial $prev;

    #[ORM\ManyToOne(targetEntity: Tutorial::class)]
    #[ORM\JoinColumn(onDelete: 'set null')]
    private ?Tutorial $next;

    /** @var ArrayCollection<int, TutorialTag> */
    #[ORM\ManyToMany(targetEntity: TutorialTag::class, inversedBy: 'tutorial')]
    #[ORM\JoinTable(name: 'tutorial_tag_rel')]
    #[ORM\OrderBy(['name' => 'ASC'])]
    private $tags;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    /**
     * @param array<TutorialTag> $tags
     */
    public function update(string $name, string $type, string $value, string $content, bool $isActive, string $lastUpdateAuthor, array $tags, ?Tutorial $prev, ?Tutorial $next) : void
    {
        $this->name             = $name;
        $this->type             = $type;
        $this->value            = $this->parseValue($type, $value);
        $this->content          = $content;
        $this->lastUpdateAuthor = $lastUpdateAuthor;

        $this->tags = $tags;

        $this->prev = $prev;
        $this->next = $next;

        $this->setIsActive($isActive);
    }

    public function getType() : string
    {
        return $this->type;
    }

    public function getValue() : string
    {
        return $this->value;
    }

    /**
     * @return ArrayCollection<TutorialTag>|array<TutorialTag>
     */
    public function getTags()
    {
        return $this->tags;
    }

    public function getPrev() : ?Tutorial
    {
        return $this->prev;
    }

    public function setPrev(?Tutorial $prev) : void
    {
        $this->prev = $prev;
    }

    public function getNext() : ?Tutorial
    {
        return $this->next;
    }

    public function setNext(?Tutorial $next) : void
    {
        $this->next = $next;
    }

    private function parseValue(string $type, string $value) : string
    {
        switch ($type) {
            case self::TYPE_VIDEO_YOUTUBE:
                return Helper::parseYouTubeTokenFromUri($value);
                break;
        }

        return $value;
    }
}
