<?php

declare(strict_types=1);

namespace Skadmin\Tutorial\Doctrine\Tutorial;

use SkadminUtils\DoctrineTraits\Facade;
use DateTimeInterface;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Nette\Utils\DateTime;
use Nettrine\ORM\EntityManagerDecorator;
use function intval;

/**
 * Class TutorialFacade
 */
final class TutorialFacade extends Facade
{
    use Facade\Webalize;
    use Facade\Sequence;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = Tutorial::class;
    }

    public function getModelForGrid() : QueryBuilder
    {
        /** @var EntityRepository $repository */
        $repository = $this->em->getRepository($this->table);
        return $repository->createQueryBuilder('a')
            ->leftJoin('a.tags', 't');
    }

    /**
     * @param TutorialTag[]|array $tags
     */
    public function create(string $name, string $type, string $value, string $content, bool $isActive, string $lastUpdateAuthor, array $tags, ?Tutorial $prev, ?Tutorial $next) : Tutorial
    {
        return $this->update(null, $name, $type, $value, $content, $isActive, $lastUpdateAuthor, $tags, $prev, $next);
    }

    /**
     * @param TutorialTag[]|array $tags
     */
    public function update(?int $id, string $name, string $type, string $value, string $content, bool $isActive, string $lastUpdateAuthor, array $tags, ?Tutorial $prev, ?Tutorial $next) : Tutorial
    {
        $tutorial = $this->get($id);
        $tutorial->update($name, $type, $value, $content, $isActive, $lastUpdateAuthor, $tags, $prev, $next);

        if (! $tutorial->isLoaded()) {
            $tutorial->setWebalize($this->getValidWebalize($name));
            $tutorial->setSequence($this->getValidSequence());
        }

        $this->em->persist($tutorial);

        if ($prev instanceof Tutorial) {
            $prev->setNext($tutorial);
        }

        if ($next instanceof Tutorial) {
            $next->setPrev($tutorial);
        }

        $this->em->flush();

        return $tutorial;
    }

    public function get(?int $id = null) : Tutorial
    {
        if ($id === null) {
            return new Tutorial();
        }

        $tutorial = parent::get($id);

        if ($tutorial === null) {
            return new Tutorial();
        }

        return $tutorial;
    }

    public function getCount(?TutorialFilter $filter = null) : int
    {
        $qb = $this->findTutorialQb($filter);
        $qb->select('count(distinct a.id)');

        return intval($qb->getQuery()
            ->getSingleScalarResult());
    }

    /**
     * @return Tutorial[]
     */
    public function findTutorial(?TutorialFilter $filter = null, ?int $limit = null, ?int $offset = null) : array
    {
        return $this->findTutorialQb($filter, $limit, $offset)
            ->orderBy('a.sequence', Criteria::ASC)
            ->getQuery()
            ->getResult();
    }

    private function findTutorialQb(?TutorialFilter $filter = null, ?int $limit = null, ?int $offset = null) : QueryBuilder
    {
        /** @var QueryBuilder $qb */
        $qb = $this->em
            ->getRepository($this->table)
            ->createQueryBuilder('a')
            ->leftJoin('a.tags', 't')
            ->distinct();

        $qb->setMaxResults($limit)
            ->setFirstResult($offset)
            ->orderBy('a.createdAt', 'DESC');

        $criteria = Criteria::create();

        if ($filter !== null) {
            $filter->modifyCriteria($criteria);
        }

        $qb->addCriteria($criteria);

        return $qb;
    }

    public function findByWebalize(string $webalize) : ?Tutorial
    {
        $criteria = ['webalize' => $webalize];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }

    public function remove(Tutorial $tutorial) : bool
    {
        if ($tutorial->isLoaded()) {
            $this->em->remove($tutorial);
            $this->em->flush();

            return true;
        }

        return false;
    }
}
