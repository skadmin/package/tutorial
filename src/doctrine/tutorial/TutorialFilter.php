<?php

declare(strict_types=1);

namespace Skadmin\Tutorial\Doctrine\Tutorial;

use SkadminUtils\DoctrineTraits\ACriteriaFilter;
use Doctrine\Common\Collections\Criteria;
use Nette\SmartObject;
use Nette\Utils\DateTime;
use function count;
use function trim;

final class TutorialFilter extends ACriteriaFilter
{
    use SmartObject;

    /** @var string */
    private $phrase = '';

    /** @var array<TutorialTag> */
    private $tags = [];

    /** @var bool|null */
    private $isActive = null;


    public function getPhrase() : string
    {
        return trim($this->phrase);
    }

    public function setPhrase(string $phrase) : self
    {
        $this->phrase = $phrase;
        return $this;
    }

    /**
     * @return array<TutorialTag>
     */
    public function getTags() : array
    {
        return $this->tags;
    }

    /**
     * @param array<TutorialTag> $tags
     */
    public function setTags(array $tags) : void
    {
        $this->tags = $tags;
    }

    public function isActive() : ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive) : void
    {
        $this->isActive = $isActive;
    }

    public function modifyCriteria(Criteria &$criteria, string $alias = 'a') : void
    {
        $expr = Criteria::expr();

        if ($this->getPhrase() !== '') {
            $criteria->andWhere($expr->orX(
                $expr->contains($this->getEntityName($alias, 'name'), $this->getPhrase()),
                $expr->contains($this->getEntityName($alias, 'content'), $this->getPhrase()),
                $expr->contains($this->getEntityName($alias, 'value'), $this->getPhrase()),
                $expr->contains($this->getEntityName($alias, 'lastUpdateAuthor'), $this->getPhrase())
            ));
        }

        if (count($this->getTags()) > 0) {
            $criteria->andWhere($expr->in('t.id', $this->getTags()));
        }

        if (is_bool($this->isActive())) {
            $criteria->andWhere(Criteria::expr()->eq($this->getEntityName($alias, 'isActive'), $this->isActive()));
        }
    }
}
