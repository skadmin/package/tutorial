<?php

declare(strict_types=1);

namespace Skadmin\Tutorial\Doctrine\Tutorial;

use Doctrine\Common\Collections\ArrayCollection;
use SkadminUtils\DoctrineTraits\Entity;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class TutorialTag
{
    use Entity\Id;
    use Entity\IsActive;
    use Entity\WebalizeName;
    use Entity\Content;
    use Entity\Sequence;

    #[ORM\Column]
    private string $color = '';

    /** @var ArrayCollection<int, Tutorial> */
    #[ORM\ManyToMany(targetEntity: Tutorial::class, mappedBy: 'tags')]
    private $tutorial;

    public function __construct()
    {
        $this->tutorial = new ArrayCollection();
    }

    public function update(string $name, string $content, string $color, bool $isActive) : void
    {
        $this->name    = $name;
        $this->content = $content;
        $this->color   = $color;

        $this->setIsActive($isActive);
    }

    public function getColor() : string
    {
        return $this->color === '' ? '#FFFFFF' : $this->color;
    }

    /**
     * @return ArrayCollection|Tutorial[]
     */
    public function getTutorial()
    {
        return $this->tutorial;
    }

}
