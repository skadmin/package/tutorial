<?php

declare(strict_types=1);

namespace Skadmin\Tutorial\Doctrine\Tutorial;

use SkadminUtils\DoctrineTraits\Facade;
use Nettrine\ORM\EntityManagerDecorator;

final class TutorialTagFacade extends Facade
{
    use Facade\Webalize;
    use Facade\Sequence;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = TutorialTag::class;
    }

    public function create(string $name, string $content, string $color, bool $isActive): TutorialTag
    {
        return $this->update(null, $name, $content, $color, $isActive);
    }

    public function update(?int $id, string $name, string $content, string $color, bool $isActive): TutorialTag
    {
        $tutorialTag = $this->get($id);
        $tutorialTag->update($name, $content, $color, $isActive);

        if (! $tutorialTag->isLoaded()) {
            $tutorialTag->setWebalize($this->getValidWebalize($name));
            $tutorialTag->setSequence($this->getValidSequence());
        }

        $this->em->persist($tutorialTag);
        $this->em->flush();

        return $tutorialTag;
    }

    public function get(?int $id = null): TutorialTag
    {
        if ($id === null) {
            return new TutorialTag();
        }

        $tutorialTag = parent::get($id);

        if ($tutorialTag === null) {
            return new TutorialTag();
        }

        return $tutorialTag;
    }

    /**
     * @return TutorialTag[]
     */
    public function getAll(bool $onlyActive = false): array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        $orderBy = ['sequence' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

    public function findByWebalize(string $webalize): ?TutorialTag
    {
        $criteria = ['webalize' => $webalize];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }
}
