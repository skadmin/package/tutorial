<?php

declare(strict_types=1);

namespace Skadmin\Tutorial\Components\Front;

/**
 * Interface IDetailFactory
 */
interface IDetailFactory
{
    public function create(int $id) : Detail;
}
