<?php

declare(strict_types=1);

namespace Skadmin\Tutorial\Components\Front;

use App\Components\Grid\TemplateControl;
use App\Model\System\APackageControl;
use SkadminUtils\ImageStorage\ImageStorage;
use Skadmin\Tutorial\BaseControl;
use Skadmin\Tutorial\Doctrine\Tutorial\Tutorial;
use Skadmin\Tutorial\Doctrine\Tutorial\TutorialFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;

/**
 * Class Detail
 */
class Detail extends TemplateControl
{
    use APackageControl;

    /** @var TutorialFacade */
    private $facade;

    /** @var Tutorial */
    private $tutorial;

    public function __construct(int $id, TutorialFacade $facade, Translator $translator)
    {
        parent::__construct($translator);
        $this->facade = $facade;

        $this->tutorial = $this->facade->get($id);
    }

    public function getTitle() : SimpleTranslation
    {
        return new SimpleTranslation('tutorial.front.detail.title - %s', $this->tutorial->getName());
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/detail.latte');

        $template->tutorial = $this->tutorial;
        $template->package  = new BaseControl();

        $template->render();
    }
}
