<?php

declare(strict_types=1);

namespace Skadmin\Tutorial\Components\Front;

/**
 * Interface IOverviewFactory
 */
interface IOverviewFactory
{
    public function create() : Overview;
}
