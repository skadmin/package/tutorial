<?php

declare(strict_types=1);

namespace Skadmin\Tutorial\Components\Front;

use App\Components\Grid\TemplateControl;
use App\Model\System\APackageControl;
use SkadminUtils\ImageStorage\ImageStorage;
use Nette;
use Nette\Application\UI\Presenter;
use Nette\ComponentModel\IContainer;
use Skadmin\Tutorial\BaseControl;
use Skadmin\Tutorial\Doctrine\Tutorial\TutorialFacade;
use Skadmin\Translator\Translator;
use Skadmin\Tutorial\Doctrine\Tutorial\TutorialFilter;
use Skadmin\Tutorial\Doctrine\Tutorial\TutorialTag;
use Skadmin\Tutorial\Doctrine\Tutorial\TutorialTagFacade;
use SkadminUtils\Core\Components\Helpful\IPaginatorFactory;
use SkadminUtils\Core\Components\Helpful\TPaginatorOverview;
use SkadminUtils\FormControls\UI\Form;

class Overview extends TemplateControl
{
    use APackageControl;
    use TPaginatorOverview;

    private const COUNT = 24;

    /** @var string */
    private $tags = '';

    /** @var TutorialFacade */
    private $facade;

    /** @var TutorialTagFacade */
    private $facadeTag;

    /** @var TutorialFilter */
    private $filter;

    public function __construct(TutorialFacade $facade, TutorialTagFacade $facadeTag, Translator $translator, IPaginatorFactory $iPaginatorFactory)
    {
        parent::__construct($translator);
        $this->facade    = $facade;
        $this->facadeTag = $facadeTag;

        $this->paginator = $iPaginatorFactory->create();

        $this->filter = new TutorialFilter();
        $this->filter->setIsActive(true);
    }

    public function setParent(?IContainer $parent, string $name = null)
    {
        parent::setParent($parent, $name);

        $this->page = $this->getParameter('page', 1);
        $this->tags = trim($this->getParameter('tags', ''));

        if ($this->tags !== '') {
            $this->filter->setTags(Nette\Utils\Arrays::map(explode(';', $this->tags), fn($tagId) => $this->facadeTag->get(intval($tagId))));
        }

        if ($this->getPresenterIfExists() instanceof Presenter) {
            $this->paginator->setPaginator(
                $this->facade->getCount($this->filter),
                self::COUNT,
                intval($this->getParameter('page', 1)),
            );
        }

        return $this;
    }

    public function getTitle() : string
    {
        return 'tutorial.front.overview.title';
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');

        $template->tutorials = $this->facade->findTutorial($this->filter, self::COUNT, $this->paginator->getOffset());
        $template->package   = new BaseControl();

        $template->render();
    }

    public function createLink() : string
    {
        $this->tags = trim($this->tags);

        return $this->link('this', [
            'page' => $this->page === 1 ? null : $this->page,
            'tags' => $this->tags === '' ? null : $this->tags,
        ]);
    }

    protected function createComponentFormFilter() : Form
    {
        $form = new Form();

        $form->addCheckboxList('tags', 'tutorial.front.overview.form', $this->facadeTag->getPairs('id', 'name', 'sequence'));

        $form->onSuccess[] = [$this, 'formFilterOnSuccess'];

        $form->setDefaults(['tags' => $this->tags !== '' ? explode(';', $this->tags) : null]);

        return $form;
    }

    public function formFilterOnSuccess(Form $form, Nette\Utils\ArrayHash $values) : void
    {
        $this->tags = implode(';', $values->tags);
        $this->filter->setTags(Nette\Utils\Arrays::map($values->tags, fn($tagId) => $this->facadeTag->get(intval($tagId))));

        if ($this->getPresenterIfExists()) {
            $this->getPresenter()->payload->url = $this->createLink();
        }

        $this->redrawControl('snipOverview');
    }
}
