<?php

declare(strict_types=1);

namespace Skadmin\Tutorial\Components\Admin;

use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\System\ABaseControl;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use SkadminUtils\ImageStorage\ImageStorage;
use Nette\Application\UI\Presenter;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Tutorial\BaseControl;
use Skadmin\Tutorial\Doctrine\Tutorial\Tutorial;
use Skadmin\Tutorial\Doctrine\Tutorial\TutorialFacade;
use Skadmin\Tutorial\Doctrine\Tutorial\TutorialTagFacade;
use Skadmin\Translator\Translator;
use SkadminUtils\Utils\Utils\Colors\Colors;
use Ublaboo\DataGrid\Column\Action\Confirmation\CallbackConfirmation;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;
use function sprintf;

/**
 * Class Overview
 */
class Overview extends GridControl
{
    use APackageControl;

    /** @var TutorialFacade */
    private $facade;

    /** @var TutorialTagFacade */
    private $facadeTutorialTag;

    /** @var LoaderFactory */
    private $webLoader;

    /** @var ImageStorage */
    private $imageStorage;

    public function __construct(TutorialFacade $facade, TutorialTagFacade $facadeTutorialTag, Translator $translator, User $user, LoaderFactory $webLoader, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);

        $this->facade            = $facade;
        $this->facadeTutorialTag = $facadeTutorialTag;
        $this->webLoader         = $webLoader;
        $this->imageStorage      = $imageStorage;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle() : string
    {
        return 'tutorial.overview.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs() : array
    {
        return [
            $this->webLoader->createJavaScriptLoader('jQueryUi'),
        ];
    }

    protected function createComponentGrid(string $name) : GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModelForGrid()
            ->orderBy('a.sequence', 'ASC'));

        // DATA
        $translator = $this->translator;
        $dialYesNo  = Arrays::map(Constant::DIAL_YES_NO, static function ($text) use ($translator) : string {
            return $translator->translate($text);
        });

        // COLUMNS
        $grid->addColumnText('name', 'grid.tutorial.overview.name')
            ->setRenderer(function (Tutorial $tutorial) : Html {
                $result = new Html();

                if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit',
                        'id'      => $tutorial->getId(),
                    ]);

                    $result->addHtml(Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ])->setText($tutorial->getName()));
                } else {
                    $result->setText($tutorial->getName());
                }

                $prevNext = [];

                if ($tutorial->getPrev() instanceof Tutorial) {
                    $prevLink = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit',
                        'id'      => $tutorial->getPrev()->getId(),
                    ]);

                    $result->addHtml(Html::el('code', ['class' => 'small text-muted d-block'])
                        ->setText($this->translator->translate('grid.tutorial.overview.name.prev: '))
                        ->addHtml(Html::el('a', ['href' => $prevLink])->setText($tutorial->getPrev()->getName())));
                }

                if ($tutorial->getNext() instanceof Tutorial) {
                    $nextLink = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit',
                        'id'      => $tutorial->getNext()->getId(),
                    ]);

                    $result->addHtml(Html::el('code', ['class' => 'small text-muted d-block'])
                        ->setText($this->translator->translate('grid.tutorial.overview.name.next: '))
                        ->addHtml(Html::el('a', ['href' => $nextLink])->setText($tutorial->getNext()->getName())));
                }

                return $result;
            });
        $grid->addColumnText('tags', 'grid.tutorial.overview.tags')
            ->setRenderer(static function (Tutorial $tutorial) : Html {
                $tags = new Html();
                foreach ($tutorial->getTags() as $tag) {
                    $color = $tag->getColor() !== '' ? $tag->getColor() : '#FFFFFF';
                    $tags->addHtml(Html::el('span', [
                        'class' => 'badge mr-1 px-2 mb-1 border border-primary',
                        'style' => sprintf('background-color: %s; color: %s', $color, Colors::getContrastColor($color)),
                    ])->setText($tag->getName()));
                }
                return $tags;
            });
        $grid->addColumnText('isActive', 'grid.tutorial.overview.is-active')
            ->setAlign('center')
            ->setReplacement($dialYesNo);

        // FILTER
        $grid->addFilterText('name', 'grid.tutorial.overview.name');
        $grid->addFilterSelect('isActive', 'grid.tutorial.overview.is-active', Constant::PROMTP_ARR + Constant::DIAL_YES_NO)
            ->setTranslateOptions();
        $grid->addFilterSelect('tags', 'grid.tutorial.overview.tags', $this->facadeTutorialTag->getPairs('id', 'name'), 't.id')
            ->setPrompt(Constant::PROMTP);

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addAction('edit', 'grid.tutorial.overview.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::DELETE)) {
            $confirmation   = new CallbackConfirmation(function (Tutorial $tutorial) {
                $translation = new SimpleTranslation('grid.tutorial.overview.action.remove.confirmation - %s', $tutorial->getName());
                return $this->translator->translate($translation);
            });
            $grid->addActionCallback('remove', 'grid.tutorial.overview.action.remove')
                ->setConfirmation($confirmation)
                ->setIcon('trash-alt')
                ->setTitle('grid.tutorial.overview.action.remove.title')
                ->setClass('btn btn-xs btn-outline-danger ajax')
                ->onClick[] = function ($id) {
                $tutorial = $this->facade->get(intval($id));

                $tutorialName = $tutorial->getName();
                if ($this->facade->remove($tutorial)) {
                    $flashMessage = new SimpleTranslation('grid.tutorial.overview.action.flash.remove.success "%s"', $tutorialName);
                    $type         = Flash::SUCCESS;
                } else {
                    $flashMessage = new SimpleTranslation('grid.tutorial.overview.action.flash.remove.danger "%s"', $tutorialName);
                    $type         = Flash::DANGER;
                }

                if ($this->getPresenterIfExists() !== null) {
                    $this->getPresenter()->flashMessage($flashMessage, $type);
                }

                $this['grid']->reload();
            };
        }

        // TOOLBAR
        $grid->addToolbarButton('Component:default#2', 'grid.tutorial.overview.action.overview-tag', [
            'package' => new BaseControl(),
            'render'  => 'overview-tag',
        ])->setIcon('tags')
            ->setClass('btn btn-xs btn-outline-primary');

        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addToolbarButton('Component:default', 'grid.tutorial.overview.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        $grid->addToolbarSeo(BaseControl::RESOURCE, $this->getTitle());

        // SORTING
        $grid->setSortable();
        $grid->setSortableHandler($this->link('sort!'));

        return $grid;
    }

    public function handleSort(?string $item_id, ?string $prev_id, ?string $next_id) : void
    {
        $this->facade->sort($item_id, $prev_id, $next_id);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $presenter->flashMessage('grid.tutorial.overview-tag.flash.sort.success', Flash::SUCCESS);
        }

        $this['grid']->reload();
    }
}
