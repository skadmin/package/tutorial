<?php

declare(strict_types=1);

namespace Skadmin\Tutorial\Components\Admin;

/**
 * Interface IOverviewTagFactory
 */
interface IOverviewTagFactory
{
    public function create() : OverviewTag;
}
