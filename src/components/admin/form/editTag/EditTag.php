<?php

declare(strict_types=1);

namespace Skadmin\Tutorial\Components\Admin;

use App\Components\Form\FormWithUserControl;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\Tutorial\BaseControl;
use Skadmin\Tutorial\Doctrine\Tutorial\TutorialTag;
use Skadmin\Tutorial\Doctrine\Tutorial\TutorialTagFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

/**
 * Class EditTag
 */
class EditTag extends FormWithUserControl
{
    use APackageControl;

    /** @var LoaderFactory */
    public $webLoader;

    /** @var TutorialTagFacade */
    private $facade;

    /** @var TutorialTag */
    private $tutorialTag;

    public function __construct(?int $id, TutorialTagFacade $facade, Translator $translator, LoggedUser $user, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);
        $this->facade    = $facade;
        $this->webLoader = $webLoader;

        $this->tutorialTag = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    /**
     * @return SimpleTranslation|string
     */
    public function getTitle()
    {
        if ($this->tutorialTag->isLoaded()) {
            return new SimpleTranslation('form.tutorial-tag.edit.title - %s', $this->tutorialTag->getName());
        }

        return 'form.tutorial-tag.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss() : array
    {
        return [
            $this->webLoader->createCssLoader('colorPicker'),
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs() : array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('colorPicker'),
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values) : void
    {
        if ($this->tutorialTag->isLoaded()) {
            $tutorialTag = $this->facade->update(
                $this->tutorialTag->getId(),
                $values->name,
                $values->content,
                $values->color,
                $values->is_active
            );
            $this->onFlashmessage('form.tutorial-tag.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $tutorialTag = $this->facade->create(
                $values->name,
                $values->content,
                $values->color,
                $values->is_active
            );
            $this->onFlashmessage('form.tutorial-tag.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'send_back') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit-tag',
            'id'      => $tutorialTag->getId(),
        ]);
    }

    public function processOnBack() : void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview-tag',
        ]);
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/editTag.latte');

        $template->tutorialTag = $this->tutorialTag;
        $template->render();
    }

    protected function createComponentForm() : Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.tutorial-tag.edit.name')
            ->setRequired('form.tutorial-tag.edit.name.req');
        $form->addTextArea('content', 'form.tutorial-tag.edit.content');
        $form->addText('color', 'form.tutorial-tag.edit.color');

        $form->addCheckbox('is_active', 'form.tutorial-tag.edit.is-active')
            ->setDefaultValue(true);

        // BUTTON
        $form->addSubmit('send', 'form.tutorial-tag.edit.send');
        $form->addSubmit('send_back', 'form.tutorial-tag.edit.send-back');
        $form->addSubmit('back', 'form.tutorial-tag.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults() : array
    {
        if (! $this->tutorialTag->isLoaded()) {
            return [];
        }

        return [
            'name'      => $this->tutorialTag->getName(),
            'content'   => $this->tutorialTag->getContent(),
            'color'     => $this->tutorialTag->getColor(),
            'is_active' => $this->tutorialTag->isActive(),
        ];
    }
}
