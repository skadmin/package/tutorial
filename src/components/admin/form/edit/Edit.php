<?php

declare(strict_types=1);

namespace Skadmin\Tutorial\Components\Admin;

use App\Components\Form\FormWithUserControl;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\Doctrine\User\User;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use SkadminUtils\ImageStorage\ImageStorage;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Arrays;
use Nette\Utils\DateTime;
use Nette\Utils\Validators;
use Skadmin\Tutorial\BaseControl;
use Skadmin\Tutorial\Doctrine\Tutorial\Tutorial;
use Skadmin\Tutorial\Doctrine\Tutorial\TutorialFacade;
use Skadmin\Tutorial\Doctrine\Tutorial\TutorialTag;
use Skadmin\Tutorial\Doctrine\Tutorial\TutorialTagFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;
use function explode;
use function is_bool;
use function sprintf;

/**
 * Class Edit
 */
class Edit extends FormWithUserControl
{
    use APackageControl;

    /** @var LoaderFactory */
    public $webLoader;

    /** @var TutorialFacade */
    private $facade;

    /** @var TutorialTagFacade */
    private $facadeTutorialTag;

    /** @var User */
    private $user;

    /** @var Tutorial */
    private $tutorial;

    public function __construct(?int $id, TutorialFacade $facade, TutorialTagFacade $facadeTutorialTag, Translator $translator, LoaderFactory $webLoader, LoggedUser $user)
    {
        parent::__construct($translator, $user);
        $this->facade            = $facade;
        $this->facadeTutorialTag = $facadeTutorialTag;
        $this->webLoader         = $webLoader;
        $this->user              = $this->loggedUser->getIdentity(); //@phpstan-ignore-line

        $this->tutorial = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    /**
     * @return SimpleTranslation|string
     */
    public function getTitle()
    {
        if ($this->tutorial->isLoaded()) {
            return new SimpleTranslation('form.tutorial.edit.title - %s', $this->tutorial->getName());
        }

        return 'form.tutorial.edit.title';
    }


    /**
     * @return JavaScriptLoader[]
     */
    public function getJs() : array
    {
        return [$this->webLoader->createJavaScriptLoader('adminTinyMce')];
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');

        $template->tutorial = $this->tutorial;
        $template->render();
    }

    protected function createComponentForm() : Form
    {
        // DATA
        $dataTutorial = $this->facade->getPairs('id', 'name');
        if ($this->tutorial->isLoaded()) {
            unset($dataTutorial[$this->tutorial->getId()]);
        }

        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.tutorial.edit.name')
            ->setRequired('form.tutorial.edit.name.req');
        $form->addTextArea('content', 'form.tutorial.edit.content');

        $form->addMultiSelect('tags', 'form.tutorial.edit.tags', $this->facadeTutorialTag->getPairs('id', 'name', 'sequence'))
            ->setTranslator(null);
        $form->addCheckbox('is_active', 'form.tutorial.edit.is-active')
            ->setDefaultValue(true);

        // $form->addSelect('type', 'form.tutorial.edit.type', Tutorial::TYPES)
        //     ->setPrompt(Constant::PROMTP)
        //     ->setRequired('form.tutorial.edit.type.req');
        $form->addText('value', 'form.tutorial.edit.value');

        // PREV && NEXT
        $form->addSelect('prev', 'form.tutorial.edit.prev', $dataTutorial)
            ->setTranslator(null)
            ->setPrompt(Constant::PROMTP)
            ->setHtmlAttribute('data-chosen-placeholder-text', $this->translator->translate('form.tutorial.edit.prev.chosen-placeholder-text'))
            ->setHtmlAttribute('data-chosen-no-result-text', $this->translator->translate('form.tutorial.edit.prev.chosen-no-result-text'));
        $form->addSelect('next', 'form.tutorial.edit.next', $dataTutorial)
            ->setTranslator(null)
            ->setPrompt(Constant::PROMTP)
            ->setHtmlAttribute('data-chosen-placeholder-text', $this->translator->translate('form.tutorial.edit.next.chosen-placeholder-text'))
            ->setHtmlAttribute('data-chosen-no-result-text', $this->translator->translate('form.tutorial.edit.next.chosen-no-result-text'));

        // BUTTON
        $form->addSubmit('send', 'form.tutorial.edit.send');
        $form->addSubmit('send_back', 'form.tutorial.edit.send-back');
        $form->addSubmit('back', 'form.tutorial.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    public function processOnSuccess(Form $form, ArrayHash $values) : void
    {
        $prev = null;
        if (Validators::isNumber($values->prev)) {
            $prev = $this->facade->get($values->prev);

            if (! $prev->isLoaded()) {
                $prev = null;
            }
        }

        $next = null;
        if (Validators::isNumber($values->next)) {
            $next = $this->facade->get($values->next);

            if (! $next->isLoaded()) {
                $next = null;
            }
        }

        if (isset($values->tags)) {
            $tags = Arrays::map($values->tags, function ($tagId) : TutorialTag {
                return $this->facadeTutorialTag->get($tagId);
            });
        }

        if ($this->tutorial->isLoaded()) {
            $tutorial = $this->facade->update(
                $this->tutorial->getId(),
                $values->name,
                Tutorial::TYPE_VIDEO_YOUTUBE,
                $values->value,
                $values->content,
                $values->is_active,
                $this->user->getFullName(),
                $tags,
                $prev,
                $next
            );
            $this->onFlashmessage('form.tutorial.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $tutorial = $this->facade->create(
                $values->name,
                Tutorial::TYPE_VIDEO_YOUTUBE,
                $values->value,
                $values->content,
                $values->is_active,
                $this->user->getFullName(),
                $tags,
                $prev,
                $next
            );
            $this->onFlashmessage('form.tutorial.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'send_back') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit',
            'id'      => $tutorial->getId(),
        ]);
    }

    public function processOnBack() : void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ]);
    }

    /**
     * @return mixed[]
     */
    private function getDefaults() : array
    {
        if (! $this->tutorial->isLoaded()) {
            return [];
        }

        $tagsId = Arrays::map($this->tutorial->getTags()->toArray(), static function (TutorialTag $tag) : ?int {
            return $tag->getId();
        });

        $next = $this->tutorial->getNext() instanceof Tutorial ? $this->tutorial->getNext()->getId() : null;
        $prev = $this->tutorial->getPrev() instanceof Tutorial ? $this->tutorial->getPrev()->getId() : null;


        return [
            'name'      => $this->tutorial->getName(),
            'content'   => $this->tutorial->getContent(),
            'value'     => $this->tutorial->getValue(),
            'is_active' => $this->tutorial->isActive(),
            'tags'      => $tagsId,
            'next'      => $next,
            'prev'      => $prev,
        ];
    }
}
