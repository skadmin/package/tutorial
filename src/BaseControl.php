<?php

declare(strict_types=1);

namespace Skadmin\Tutorial;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE  = 'tutorial';
    
    public function getMenu() : ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fab fa-fw fa-leanpub']),
            'items'   => ['overview'],
        ]);
    }
}
